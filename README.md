## ROID RADAR ##

ROID RADAR be warnin' you about nearby asteroids - and also about prospecting potential of rocks close to earth. This is especially helpful if your name is Richard Branson or James Cameron.

## Setup ##

* Clone this repo.
* `$ npm install & bower install`
* `$ grunt build`
* `$ node app.js`